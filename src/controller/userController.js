
const { database } = require('../database/database.js'); 
const { getHashMD5, isEmail, isValidCPF } = require('../helpers/stringHelper.js');

class userController {
    constructor () {
        this.initDatabase();
    };

    initDatabase = async () => {
        await database().init();
        this.datasource = database().getDatasource();
        this.userDatabase = this.datasource.getRepository("User");
        this.postDatabase = this.datasource.getRepository("UserPosts");
    };

    createUser = async (req, res) => {
        const newUser = this.getUserBodyToJSON(req.body);
        console.log("Requisição de criação de usuário recebida: ", newUser);
        
        let finalizeResponse = {
            canAdd: true,
            message: "",
        };

        await this.userDatabase
            .findOne({ where: { email: newUser.email } })
            .then((user) => {
                console.log("Verificando se o email já está cadastrado...");
                if (user !== null) {
                    finalizeResponse = {
                        canAdd: false,
                        message: "Email já cadastrado..."  
                    };  
                };
            });

        await this.userDatabase
            .findOne({ where: { userCpf: newUser.userCpf } })
            .then((user) => {
                if (user !== null) {
                    finalizeResponse = {
                        canAdd: false,
                        message: "CPF já cadastrado..."
                    };
                };
            });

        await this.userDatabase
            .findOne({ where: { profileName: newUser.profileName } })
            .then((user) => {
                if (user !== null) {
                    finalizeResponse = {
                        canAdd: false,
                        message: "Nome de perfil já cadastrado..."
                    };
                };
            }); 

        if (isEmail(newUser.email) === false) {
            finalizeResponse = {
                canAdd: false,
                message: "Email inválido..."
            };
        };

        if (isValidCPF(newUser.userCpf) === false) {
            finalizeResponse = {
                canAdd: false,
                message: "CPF inválido..."
            };
        };

        if (finalizeResponse.canAdd) {
            await this.userDatabase
                .save(newUser)
                .then((savedUser) => {
                    console.log("Usuário cadastrado com sucesso...", savedUser);

                    res.status(201).json({
                        message: "Usuário cadastrado com sucesso...",
                        code: 201,
                        user: savedUser
                    });
                })
                .catch((error) => {
                    console.log("Erro ao cadastrar usuário...");

                    res.status(404).json({
                        message: "Erro ao cadastrar usuário...",
                        code: 404,
                        error: error
                    }).end();
                });        
        } else { 
            console.log("Não é possível cadastrar o usuário...");
            console.log("Motivo: ", finalizeResponse.message);

            res.status(404).json({
                message: finalizeResponse.message,
                code: 404,
            }).end();
        };
    };

    getUserBodyToJSON = (user) => {
       return {
            nameUser: user.nameUser,
            profileName: user.profileName,
            accountType: user.accountType,
            userCpf: user.userCpf,
            email: user.email,
            password: getHashMD5(user.password),
        };
    };

    userLogin = async (req, res) => {
        console.log("Requisição de login recebida: ", req.body);
        let canLoggin = true;

        if (isEmail(req.body.login)) {
            console.log("Login com email");
            canLoggin = await this.existEmail(req.body.login).then((result) => result);

            if (canLoggin) {
                this.userDatabase
                    .findOne({ where: { email: req.body.login , password: getHashMD5(req.body.password)} })
                    .then((userLogin) => {
                        if (userLogin !== null) {
                            console.log("login feito com sucesso", userLogin.profileName);

                            res.status(200).json({
                                message: "Login feito com sucesso",
                                code: 200,
                                profileName: userLogin.profileName,
                                userId: userLogin.id,
                            }).end();
                        } else {
                            console.log("Senha incorreta...");

                            res.status(404).json({
                                message: "Senha incorreta...",
                                code: 404
                            }).end();
                        };
                    })
                    .catch((error) => {
                        console.log("Ocorreu uma inconsistência ao fazer login...", error);
                        
                        res.status(404).json({
                            message: "Ocorreu uma inconsistência ao fazer login...",
                            code: 404,
                            error: error
                        }).end();
                    });
            } else {
                console.log("Email não cadastrado...");

                res.status(404).json({
                    message: "Email não cadastrado...",
                    code: 404
                }).end();
            };
        } else {
            console.log("Login com Nome de Perfil");
            canLoggin = await this.existProfileName(req.body.login).then((result) => result); 

            if (canLoggin) {
                this.userDatabase
                    .findOne({ where: { profileName: req.body.login , password: getHashMD5(req.body.password)} })
                    .then((userLogin) => {
                        if (userLogin !== null) {
                            console.log("login feito com sucesso", userLogin.profileName);

                            res.status(200).json({
                                message: "Login feito com sucesso",
                                code: 200,
                                profileName: userLogin.profileName,
                                userId: userLogin.id,
                            }).end();
                        } else {
                            console.log("Senha incorreta...");

                            res.status(404).json({
                                message: "Senha incorreta...",
                                code: 404
                            }).end();
                        };
                    })
                    .catch((error) => {
                        console.log("Ocorreu uma inconsistência ao fazer login...", error);

                        res.status(404).json({
                            message: "Ocorreu uma inconsistência ao fazer login...",
                            code: 404,
                            error: error
                        }).end();
                    });
            } else {
                console.log("Nome de perfil não cadastrado...");

                res.status(404).json({
                    message: "Nome de perfil não cadastrado...",
                    code: 404
                }).end();
            };
        };
    };

    existEmail = (email) => {
        return this.userDatabase
            .findOne({ where: { email: email } })
            .then((user) => {
                return (user !== null);
            });
    };

    existProfileName = (profileName) => {
        return this.userDatabase
            .findOne({ where: { profileName: profileName } })
            .then((user) => {
                return (user !== null);
            });
    };

    userPost = async (req, res) => {
        const userPost = this.getUserPostToJSON(req.body);
        console.log("Requisição de postagem recebida: ", userPost);

        if (await this.userDatabase.findOne({ where: { id: userPost.userId } }) !== null) {
            this.postDatabase
                .save(userPost)
                .then(post => {
                    if (post !== null) {
                        console.log("Postagem feita com sucesso...", post);

                        res.status(201).json({
                            message: "Postagem feita com sucesso...",
                            code: 201
                        }).end();
                    };
                }) 
                .catch((error) => {
                    console.log("Erro ao postar...", error);

                    res.status(404).json({
                        message: "Erro ao postar...",
                        code: 404,
                        error: error
                    }).end();
                });
        } else {
            console.log("Usuário não existe...");

            res.status(404).json({
                message: "Usuário não existe...",
                code: 404
            }).end();
        };
    };

    getUserPostToJSON = (userPost) => {
        return {
            userId: userPost.userId,
            titlePost: userPost.titlePost,
            softwares: userPost.softwares,
            styles: userPost.styles,
            projects: userPost.projects,
            types: userPost.types,
            descriptionPost: userPost.descriptionPost,
            imagePost: userPost.imagePost,
        };
    };
};

module.exports = {
    userController
};