
const EntitySchema = require("typeorm").EntitySchema

module.exports = new EntitySchema({
    name: "UserPosts",               // Nome que vincula a entidade e a tabela
    tableName: "user_posts",   
    columns: {
        id: {
            primary: true,
            type: "integer",
            generated: true,
        },
        userId: {
            type: "integer",
        },
        titlePost:{
            type: "varchar",
        },
        softwares:{
            type: "varchar",
        },
        styles: {
            type: "varchar",
        },
        projects: {
            type: "varchar",
        },
        types: {
            type: "varchar",
        },
        descriptionPost: {
            type: "varchar",
        },
        imagePost:{
            type: "varchar",
        },
    },
    relations: {
        user: { 
            target: "User",
            type: "many-to-one",
            joinColumn: { name: "userId" }
        }
    }
})
