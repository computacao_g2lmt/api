
const express = require('express');
const { userRouter } = require('./routers/userRouter.js'); 
const { database } = require('./database/database.js');
const cors = require('cors');

const app = express();
const port = 3050;

// app.use(express.json());
app.use(express.json({ limit: '100mb' }))
app.use(express.urlencoded({ limit: '100mb', extended: true }))
app.use(cors());
app.use('/user', userRouter);

app.listen(port, async () => {
    const startTime = new Date();
    console.log(`API online => http://3.22.240.190:${port}`);
    // console.log(`Server is running on http://localhost:${port}`);
    await database().init();

    setInterval(() => {        
        const currentTime = new Date();
        const uptime = Date.now() - startTime;
        const uptimeInMinutes = Math.floor(uptime / 1000 / 60);

        console.log('Tempo de atividade:', uptimeInMinutes, 'minutos');
    }, 180000); 
});